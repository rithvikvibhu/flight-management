import pickle

'''This module handles all file related actions'''

def getFlights(flight=None):
    '''Returns a list of all flights if no argument is given
    If "flight" argument is provided, only that flight is returned if found (None if not found)'''
    with open("data/flights", "r+") as file:
        flights = pickle.load(file)
        if flight:
            if flight <= len(flights):       # Check if index is within range
                return flights[flight-1]
            else:
                return None
    return flights

def getAirports(airport=None):
    '''Returns a list of all airports if no argument is given
    If "airport" argument is provided, only that airport is returned if found (None if not found)'''
    with open("data/airports", "r+") as file:
        airports = pickle.load(file)
        if airport:
            if airport <= len(airports):     # Check if index is within range
                return airports[airport-1]
            else:
                return None
    return airports

def getBookings(bookingRef=None):
    '''Returns a list of all bookings if no argument is given
    If "bookingRef" argument is provided, only that booking is returned if found (None if not found)'''
    with open("data/bookings", "r+") as file:
        bookings = pickle.load(file)
    if bookingRef:
        for b in bookings:
            if b.bookingRef == bookingRef:
                return b
        return False
    else:
         return bookings

def storeNewBooking(newBooking):
    '''Stores new booking'''
    # Read current bookings and store in temporary list
    with open("data/bookings", "r+") as file:
        bookings = pickle.load(file)
    
    # Append newBooking to list of bookings
    bookings.append(newBooking)
    
    # Store new list of bookings back in file
    with open("data/bookings", "w+") as file:
        pickle.dump(bookings, file)