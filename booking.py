import pickle, os, random

import data
from flight import Flight

def clear():
    '''This function clears the screen'''
    print "\n"*50

class Booking(object):
    '''This class handles all booking related functions'''
    
    def __init__(self, bookingRef, origin, dest, flight, passengers):
        self.bookingRef = bookingRef
        self.origin = origin
        self.dest = dest
        self.flight = flight
        self.passengers = passengers
    
    @staticmethod
    def newBooking():
        '''Creates a new booking'''
        clear()
        
        # Show table with all airports
        listAirports()
        # Get input for origin airport (and make sure it is valid)
        valid = False
        while not valid:
            origin = data.getAirports(int(raw_input('Select origin: ')))
            if origin:
                valid = True
                origin = origin.location
            else:
                print 'Invalid number. Try again.'
        clear()
        
        # Show table with all airports
        listAirports()
        # Get input for destination airport (and make sure it is valid)
        valid = False
        while not valid:
            dest = data.getAirports(int(raw_input('Select destination: ')))
            if dest:
                valid = True
                dest = dest.location
            else:
                print 'Invalid number. Try again.'
        clear()
        
        # Make sure origin and destination aren't the same
        # If it is, return with 'save-origin-dest'.
        if origin == dest:
            return 'same-origin-dest'
        
        # Display list of all flights
        Flight.displayFlights(To=dest, From=origin)
        # Get input for flight number and make sure it is valid
        valid = False
        while not valid:
            flight = data.getFlights(int(raw_input('Select flights: ')))
            if flight:
                valid = True
                flight = flight.name
            else:
                print 'Invalid number. Try again.'
        
        # Generate random 4 digit number for booking reference
        bookingRef = random.randint(1000,9999)
        
        # Get passenger details
        passengers = []
        noofpassengers = int(raw_input('How many passengers? '))
        # Ask details for each passenger
        for i in range(noofpassengers):
            print 'Passenger', str(i+1)
            name = raw_input('Name: ')
            age = int(raw_input('Age: '))
            food = raw_input('Food: ')
            # Append the dictionary to the list of passengers
            passengers.append({"name": name, "age": age, "food": food})
        
        # Create a new object of Booking with these details
        newBooking = Booking(bookingRef, origin, dest, flight, passengers)
        
        # Store this object
        data.storeNewBooking(newBooking)
        
        # Give success message and return True to go back
        print "Done! Your booking reference number is", bookingRef
        raw_input('Press enter to continue')
        return True
    
    @staticmethod
    def displayBookingDetails():
        '''Displays booking details after asking for the reference numbers'''
        
        # Ask for reference number
        bookingRef = int(raw_input('Enter your booking reference number: '))
        
        # Get booking details
        booking = data.getBookings(bookingRef)
        
        # If valid booking
        if booking:
            print 'Your bookings details:'
            print 'Booking #:', booking.bookingRef, '\t\t', 'Flight:', booking.flight
            print 'Origin:', booking.origin, '\t\t\t', 'Destination:', booking.dest
            print 'Passengers:'
            c = 1
            print '-----------------------------------------'
            print '| NO | Name \t\t| Age \t| Food \t|'
            print '|---------------------------------------|'
            for p in booking.passengers:
                print '|', c, ' |', p['name'], '\t\t|',  p['age'], '\t|',  p['food'], '\t|'
                c += 1
            print '-----------------------------------------'
        
        # If invalid number
        else:
            print 'Invalid booking reference. Try again.'
        raw_input('Press enter to continue')
        return

def listAirports():
    '''List all airports in a table'''
    
    # Get airports details
    airports = data.getAirports()
    
    # List them in a table
    c = 1
    print '-----------------------------------------------------------------'
    print '| NO | Airport \t\t\t\t\t\t| Loc \t|'
    print '|---------------------------------------------------------------|'
    for airport in airports:
        print '|', c, ' |', airport.name, '\t|',  airport.location, '\t|'
        c += 1
    print '-----------------------------------------------------------------'
