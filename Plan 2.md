# Tasks

----------------------------

## Classes

 - Flight - Rithvik
 - Booking - Ankit
 - Airport - Tapasya
 - Data Handler - Baibhav


#### Airport  - Tapasya

Properties

 - Name
 - Location
 - ~~Choose flight~~ `[All these go under book method of Airport]`
 - ~~Enter Details (taken from class flight)~~
 - ~~Add food (optional)~~
 - ~~Payment (by website's wallet)~~
 - ~~Confirm Booking~~

Methods

- getFlights ( )
	- Return List of Flight objects (only flights with destination as airport)
- book ( )
	- Ask for details and create a new `Booking`
	- Store the new `Booking` with `Data Handler`


#### Flight - Rithvik

Properties

- `name` (string) - Flight name
- `number` (string) - Flight Identity
- `airline` (string) - Airline
- `model` (string) - Model Name
- `status` (dict) - `{"status": <status>, "at": <airport>}`
- `foodAvailable` (bool) - Food availability
- `bookings` (list) - List of Booking objects

`<status>` = Boarding | Security Check | Scheduled | Arrived | Delayed | Canceled | In Air

Methods

- changeState (`new_state`)
- Fly (`to`)


#### Booking - Ankit

Properties

- `origin` (string) - Flight origin
- `destination` (string) - Flight Destination
- `passengers` (list) - `{"title": "", "name": "", "age": 0, "dob": 0, "sex": "", "food": true}`
