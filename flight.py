import pickle, os

import data

def clear():
    '''This function clears the screen'''
    print "\n"*50

class Flight(object):
    '''This class handles Flight related details'''
    
    def __init__(self, name, airline, status):
        self.name = name
        self.airline = airline
        self.status = status
    
    @staticmethod
    def displayFlights(airport=None, To=None, From=None):
        '''Lists all flights in a nice table'''
        
        # Get list of all flights
        flights = data.getFlights()
        
        # Show flights in a table
        c = 1
        print '---------------------------------------------------------'
        print '| NO | Name \t\t| Status \t| From \t| To \t|'
        print '|-------------------------------------------------------|'
        for flight in flights:
            # This checks if any condition is given as an argument
            if airport or To:
                # If provided, only flights related to that airport is shown (i.e. from, to, at of Flight)
                if airport and (airport.location not in [flight.status['to'], flight.status['from'], flight.status['at']]):
                    continue
            
                # If 'to' and 'from' args are provided, only list those matching
                if To and (To != flight.status['to'] or From != flight.status['from']):
                    continue
                    
            print '|', c, ' |', flight.name, '\t|',  flight.status['status'], '\t|', flight.status['from'], '\t|', flight.status['to'], '\t|'
            c += 1
        print '---------------------------------------------------------'
        return