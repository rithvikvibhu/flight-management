# Flight Management

---
#### Features

- Present in all major cities of India
- Expanding to greater heights
- Pre-book and enjoy your meals onboard
- 1 million happy customers
- Special/Disability assistance
- GST Ready

#### Flight
- Number
- Airlines
  - FlyAir
    - Fly high to reach the sky
  - Vista
  - Radeon Airlines
- Model
  - Calcesia 2313
- Status
  - Boarding
  - Security Check
  - Scheduled
  - Arrived
  - Delayed
  - Canceled
- Food Availability

#### Booking(s)
- Book
  - Guidelines
  - Check date
  - Web check-in
- Cancel
- Check status

#### Airports
- List of airports
  - BOM
  - DEL
  - HYD
  - BLR
  - BBSR
  - GOA

#### Routes
