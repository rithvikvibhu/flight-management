import pickle, os

# Create 'data' folder if it doesn't exist
if not os.path.exists('data'):
    os.makedirs('data')

def clear():
    '''This function clears the screen'''
    print "\n"*50

import loaddata
import data
from flight import Flight
from booking import Booking
from airport import Airport

while True:
    clear()
    print '''
    ----------------------------------
    Flight Management System
    Class 12 Project
    ----------------------------------
    Which Airport are you at?
    '''
    
    # List all airports
    Airport.listAirports()
    
    print '''For more details on what's going on behind the scenes,
    select 0'''
    
    # Get input for airport number and check and continue only if valid
    valid = False
    while not valid:
        selectedAirport = raw_input('Enter number: ')
        if selectedAirport.isdigit():
            valid = True
        else:
            print "Invalid choice."
    
    # Clear the screen
    clear()
    
    if selectedAirport == '0':
        # Info screen
        help(Airport)
        help(Flight)
        help(Booking)
        help(data)
        continue
    
    # Display the airport board menu
    Airport.displayBoard(int(selectedAirport))