import pickle

from flight import Flight
from airport import Airport
from booking import Booking

# Creates Airports
airports = []
airports.append(Airport('Bengaluru International Airport\t\t', 'BLR'))
airports.append(Airport('Chhatrapati Shivaji International Airport', 'BOM'))
airports.append(Airport('Indira Gandhi International Airport\t', 'DEL'))
airports.append(Airport('Rajiv Gandhi International Airport\t', 'HYD'))
# airports.append(Airport('Visakhapatnam Airport\t\t\t', 'VTZ'))
# airports.append(Airport('Trivandrum International Airport\t\t', 'TRV'))

with open("data/airports", "w+") as file:
    pickle.dump(airports, file)

# Creates Flights
flights = []
flights.append(Flight('AirAsia 345', 'AirAsia', {"status": "Boarding", "at": "BLR", "to": "BOM", "from": "BLR"}))
flights.append(Flight('Vista 973', 'Vista', {"status": "Security Chk", "at": "BLR", "to": "DEL", "from": "BLR"}))
flights.append(Flight('Almonde 735', 'Almonde', {"status": "In Air", "at": "", "to": "BOM", "from": "DEL"}))
flights.append(Flight('Qatar 683', 'Qatar', {"status": "Security Chk", "at": "DEL", "to": "BLR", "from": "DEL"}))
flights.append(Flight('AirIndia 232', 'AirAsia', {"status": "Boarding", "at": "BOM", "to": "DEL", "from": "BOM"}))
flights.append(Flight('Silkair 759', 'Silkair', {"status": "Boarding", "at": "BOM", "to": "BLR", "from": "BOM"}))
flights.append(Flight('AirAsia 338', 'AirAsia', {"status": "Arrived", "at": "BLR", "to": "BLR", "from": "HYD"}))
flights.append(Flight('Vista 302', 'Vista', {"status": "In Air", "at": "", "to": "HYD", "from": "BLR"}))
flights.append(Flight('Almonde 262', 'Almonde', {"status": "Arrived", "at": "DEL", "to": "DEL", "from": "HYD"}))
flights.append(Flight('Qatar 392', 'Qatar', {"status": "Security Chk", "at": "HYD", "to": "BOM", "from": "HYD"}))
with open("data/flights", "w+") as file:
    pickle.dump(flights, file)

# Creates Bookings
bookings = []
bookings.append(Booking(1234, 'BLR', 'BOM', 'F2', [{"name": "P1", "age": 35, "food": "Yes"}]))
bookings.append(Booking(4321, 'BOM', 'DEL', 'F1', [{"name": "P2", "age": 40, "food": "No"}]))
with open("data/bookings", "w+") as file:
    pickle.dump(bookings, file)