import pickle, os

import data
from flight import Flight
from booking import Booking

def clear():
    '''This function clears the screen'''
    print "\n"*50

class Airport(object):
    '''This class contains code related to Airport'''
    
    def __init__(self, name, location):
        self.name = name
        self.location = location
    
    @staticmethod
    def listAirports():
        '''Displays a list of airports in a table'''
        
        # Get a list of Airports
        airports = data.getAirports()
        c = 1
        print '-----------------------------------------------------------------'
        print '| NO | Airport \t\t\t\t\t\t| Loc \t|'
        print '|---------------------------------------------------------------|'
        for airport in airports:
            print '|', c, ' |', airport.name, '\t|',  airport.location, '\t|'
            c += 1
        print '-----------------------------------------------------------------'
    
    @staticmethod
    def displayBoard(airport):
        '''Display menu for specific airport'''
        
        # Get specific airport details
        airport = data.getAirports(airport)
        
        # Keep repeating this menu
        while True:
            clear()
            print 'Welcome to', airport.name + '!'
            print '''What would you like to do?
            1. View flights
            2. New booking
            3. View your booking details
            4. Return to previous menu'''
            
            # Do not continue until input is number in [1,5)
            valid = False
            while not valid:
                selectedAction = raw_input('Enter number: ')
                if selectedAction.isdigit() and int(selectedAction) in range(1, 5):
                    selectedAction = int(selectedAction)
                    valid = True
            clear()
            valid = False
            
            # If option selected is View Flights
            if selectedAction == 1:
                # Call displayFlights method of Flight class
                Flight.displayFlights(airport)
                raw_input('Press enter to continue')
            
            # If option selected is New Booking
            elif selectedAction == 2:
                # Call newBooking method of Booking class
                # If the returned value is same-origin-dest (an error),
                # Print an error and continue
                if Booking.newBooking() == 'same-origin-dest':
                    print 'Same origin and destination. Try again.'
                    raw_input('Press enter to continue')
                    continue
            
            # If option selected is to View Booking Details
            elif selectedAction == 3:
                # Simply call displayBookingDetails method of Booking class
                Booking.displayBookingDetails()
            
            # If option selected is to Return to previous menu,
            # break out of this loop (the airport one, not the loop in main.py)
            elif selectedAction == 4:
                break
